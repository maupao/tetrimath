const divConteneur = document.getElementById('conteneur');
const boutonsGauche = document.getElementById('boutons-gauche');
const boutonsDroite = document.getElementById('boutons-droite');
const boutonPanneau = document.getElementById('bouton-panneau');
const panneau = document.getElementById('panneau');
const boutonsPanneau = document.querySelectorAll('.bouton-panneau');
const darkbox = document.getElementById('darkbox');
const divApropos = document.getElementById('apropos');
const divAccueil = document.getElementById('accueil');
const divConsigne = document.getElementById('consigne');
const divPause = document.getElementById('pause');
const divChoix = document.getElementById('choix');
const spanTotal = document.getElementById('etiquette-total');
const divFin = document.getElementById('fin');
const educajou = document.getElementById('educajou');
const divScore = document.getElementById('score');
const divScoreFinal = document.getElementById('score-final');
const divScoreFinalNum= document.getElementById('score-final-num');
const imageExemple = document.getElementById('image-exemple');
const checkboxMusique = document.getElementById('checkbox-musique');
const checkboxEffets = document.getElementById('checkbox-effets');


// Fichiers sons
let sonMusique = new Audio('sons/musique.mp3');
let sonScore = new Audio('sons/score.mp3');
let sonBulle = new Audio('sons/bulle.mp3');
let sonWoosh = new Audio('sons/woosh.mp3');

// Listes blocs
let blocs10 = [0,1,2,3,4,5,6,7,8,9,10];
let blocs20 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20];
let blocs100 = [0,10,15,20,25,30,35,40,45,50,55,60,65,70,75,80,85,90,100];

function tirerDans(tableau) {    
    const indexAleatoire = Math.floor(Math.random() * tableau.length);    
    return tableau[indexAleatoire];
}

// Ce préfixe sera utilisé pour le stockage des paramètres dans le cache
let prefixeAppli = 'tetrimath';

// Est-on dans Openboard ?
openboard = Boolean(window.widget || window.sankore);
console.log('Openboard ? '+openboard);

// Adaptations de l'interface pour Openboard
if (openboard){
    document.body.classList.add('openboard');
    educajou.href='https://educajou.forge.apps.education.fr/';
}

// Réglages de départ et letiables globales
boutonsPanneau.forEach(bouton => {
    bouton.classList.remove('actif');
});
let panneauVisible = false;
let delaiAnimation = 100;
let blocActif = null;
let compatible = false;
let jeuEnCours = false;
let vitesseDescente = 2; // Nombre de pixels à descendre
let score = 0;
let total = 10;
let pauseEnCours = false;
let pret=false;
let intervalle = 50; // Intervalle en millisecondes (0.05 seconde)
let acceleration = false;
let deplacementLateralEnCours = false;
let musiqueOn = true;
let effetsOn = true;
let surprise = false;
let choix = false;

// On vérifie si des paramètres sont dans l'URL
function checkUrl() {
    console.log('Lecture URL');
    let url = window.location.search;
    let urlParams = new URLSearchParams(url);

    // Primtux
    if (urlParams.get('primtuxmenu')==="true") {
        educajou.style.display='none';
    }
}

function appliqueReglages() {
    console.log('Application des réglages');
    console.log('musiqueOn='+musiqueOn);
    console.log('effetsOn='+effetsOn);

    checkboxMusique.checked = musiqueOn;
    checkboxEffets.checked = effetsOn;    
}

async function verifieStockageLocal() {
    console.log('Lecture stockage local');

    if (await litDepuisStockage('musique-on')=='false'){
        console.log('MusiqueOn est false')
        musiqueOn = false;
    }
    if (await litDepuisStockage('effets-on')=='false'){
        effetsOn = false;
    }
}

async function litDepuisStockage(cle) {
    console.log('Lecture de la clé '+cle);

if (openboard){ //Récupération pour Openboard

    valeurAretourner = await window.sankore.async.preference(prefixeAppli+'-'+cle);
    console.log("lecture "+cle+"="+valeurAretourner); // Pour la console

  } else { // Récupération en Web

    valeurAretourner = localStorage.getItem(prefixeAppli+'-'+cle);
    console.log("lecture depuis stockage "+cle+"="+valeurAretourner); // Pour la console

  } 

  return valeurAretourner;
}

function stocke(cle,valeur){

    console.log("stockage "+cle+"="+valeur);

    if (openboard){
    window.sankore.setPreference(prefixeAppli+'-'+cle,valeur);

    } else {
    localStorage.setItem(prefixeAppli+'-'+cle,valeur);
    }    
}


// Début du programme
async function executeFunctions() {
    await verifieStockageLocal();
    await checkUrl();
    await appliqueReglages();
}
executeFunctions();
///////////////////////

let audioBuffer; // Variable pour stocker le buffer audio préchargé

function prechargerSon(cheminFichier) {
  return fetch(cheminFichier)
    .then(response => response.arrayBuffer())
    .then(buffer => {
      return new Promise(resolve => {
        audioContext.decodeAudioData(buffer, decodedBuffer => {
          audioBuffer = decodedBuffer;
          resolve();
        });
      });
    })
    .catch(error => {
      console.error('Erreur lors du préchargement du fichier audio', error);
    });
}

// Fonction pour jouer un son
function joueSon(son, enBoucle) {
    son.loop = enBoucle; // Définit si le son doit être joué en boucle ou pas
    son.play(); // Joue le son
}

// Fonction pour arrêter tous les sons
function arreteSons() {
    sonMusique.pause(); // Pause la musique
    sonScore.pause(); // Pause le son du score
}


function active(bouton,valeur){
    outilsPlusMoins.forEach(bouton => {
        bouton.classList.remove('actif')
    });
    bouton.classList.add('actif');
    sensPlusMoins = valeur;
}

function changeMusique(valeur){
    musiqueOn = valeur;
    stocke('musique-on',musiqueOn);
    if (jeuEnCours) {
        if (musiqueOn) {
            joueSon(sonMusique,true);
        } else {
            arreteSons();
        }
    }
}

function changeEffets(valeur){
    effetsOn = valeur;
    stocke('effets-on',effetsOn);
}

function visibilitePanneau() {
    if (panneauVisible){
        panneau.style.right=null;
        boutonPanneau.style.right=null;     
    } else {
        panneau.style.right='0px';
        boutonPanneau.style.right='calc(100% + 5px)';
    }
    panneauVisible=!panneauVisible;
}




function clic(event){

    let cible = event.target;

    // Fermer le panneau s'il est ouvert
    if (panneauVisible && !objetOuEnfantDe(cible,panneau) && cible!=boutonPanneau && !cible.classList.contains('bouton-options')){
        visibilitePanneau();
    }

}

function objetOuEnfantDe(cible, objet) {
    // Vérifie si la cible est égale à l'objet
    if (cible === objet) {
        return true;
    }
    // Vérifie si la cible est un descendant de l'objet
    let parent = cible.parentNode;
    while (parent) {
        if (parent === objet) {
            return true;
        }
        parent = parent.parentNode;
    }
    // Si aucun cas n'est vérifié, retourne false
    return false;
}

// Évènements tactiles
document.addEventListener("touchstart", clic);
// empêcher le scroll tactile
document.addEventListener('touchmove', function(event) {
    event.preventDefault();
}, { passive: false });


// Évènements souris
document.addEventListener("mousedown", clic);


// Fonctionnement des lightbox
function ouvre(div){
    div.classList.remove('hide');
    darkbox.classList.remove('hide');
}
function ferme(div){
    div.classList.add('hide');
    darkbox.classList.add('hide');
}

function changeTheme(){
    pret=false;
    ferme(divFin);
    ouvre(divAccueil);
}

function lanceConsigne(valeur){

    total = valeur;
    spanTotal.innerHTML = total;
    if (valeur === 10 ) {tableauEnCours = blocs10}
    else if (valeur === 20 ) {tableauEnCours = blocs20}
    else if (valeur === 100 ){tableauEnCours = blocs100}

    pret=true;

    imageExemple.src = 'images/exemple'+total+'.png'

    ferme(divAccueil);
    ouvre(divConsigne);
}

// Début du jeu
function debut(){
    console.log('Début du jeu');
    divConteneur.style.backgroundColor=null;
    if (musiqueOn) {joueSon(sonMusique,true);}
    jeuEnCours = true;
    score = 0;
    divScore.style.width = '0%';
    divScoreFinal.style.width = '0%';
    vitesseDescente = 2;
    let blocs = divConteneur.querySelectorAll('.bloc');
    blocs.forEach (bloc => {
        bloc.remove();
    });
    creeBloc();
}

function changeScore(valeur){
    score += valeur;

    if (score > 100){score = 100;}
    
    divScore.style.width = score + '%';

    if (score === 100){
        finDuJeu();
    }
}

function creeBloc(){
    if (!blocActif){

        if (!surprise){ // Évite le cas deux fois de suite
            surprise = pileOuFace(0.1); // 1 chance sur 10
        } else {
            surprise = false;
        }
        if (!choix && !surprise){ // Évite le cas deux fois de suite et le cumul avec surprise
            choix = pileOuFace(0.1);
        } else {
            choix = false;
        }

        let nouveauBloc=document.createElement('div');
        blocActif = nouveauBloc;        
        nouveauBloc.classList.add('bloc');
        let conteneurWidth = divConteneur.offsetWidth; // Largeur du conteneur
        let positionLeft = Math.floor(Math.random() * (conteneurWidth - 100)); // Position aléatoire entre 0 et largeur du conteneur - 100px
        nouveauBloc.style.left = positionLeft + 'px';

        if (surprise){
            nouveauBloc.innerHTML='?';
            nouveauBloc.classList.add('surprise');
        } else if (choix){
            nouveauBloc.classList.add('choix');
            nouveauBloc.addEventListener('click', function() {
                choixValeur(nouveauBloc);
                ouvre(divChoix);
                nouveauBloc.removeEventListener('click', arguments.callee);
            });
        }
         else {
            nouveauBloc.innerHTML=tirerDans(tableauEnCours);
        }

        console.log('------------');
        console.log('####### Nouveau bloc : '+blocActif.innerHTML);
        nouveauBloc.style.top='-100px';
        divConteneur.appendChild(nouveauBloc);        
        gravite(nouveauBloc);
    }

}

function choixValeur(bloc) {
    let contenuLightbox = divChoix.querySelector('.contenu-lightbox');
    // Supprimer tout contenu précédent dans la div contenuLightbox
    contenuLightbox.innerHTML = '';

    // Créer des boutons pour chaque valeur dans tableauEnCours
    tableauEnCours.forEach(function(valeur, index) {
        // Créer un bouton
        let bouton = document.createElement('button');
        bouton.classList.add('bouton-valeur-choix');
        bouton.textContent = valeur;

        // Ajouter un gestionnaire d'événements pour le clic sur le bouton
        bouton.addEventListener('click', function() {
            ferme(divChoix);
            bloc.innerHTML = valeur;
            bloc.classList.remove('choix');
            verifierPosition(bloc,true);
        });

        // Ajouter le bouton à la div contenuLightbox
        contenuLightbox.appendChild(bouton);

        // Insérer un saut de ligne après chaque deux boutons
        if ((index + 1) % 4 === 0) {
            contenuLightbox.appendChild(document.createElement('br'));
        }
    });
}


function pileOuFace(probabiliteTrue) {
    // Génère un nombre aléatoire entre 0 et 1
    var rand = Math.random();    
    // Vérifie si le nombre aléatoire est inférieur à la probabilité donnée
    if (rand < probabiliteTrue) {
        return true; // Retourne true si le nombre aléatoire est inférieur à la probabilité
    } else {
        return false; // Retourne false sinon
    }
}

function finDuJeu () {
    jeuEnCours = false;
    console.log("Fin");
    console.log("score "+score);    
    arreteSons();
    divScoreFinalNum.innerHTML=0;
    ouvre(divFin);        
    setTimeout(() => {        
        divScoreFinal.style.width = score + '%';
        if (score > 0){
            if (effetsOn){joueSon(sonScore,false);}
            // Définit l'intervalle en millisecondes pour chaque itération de l'animation.
            let interval = 1500 / score;
            console.log('intervalle '+interval+'ms');
            // Initialisez le compteur
            let counter = 0;
            // Démarrez l'animation
            let animationInterval = setInterval(function() {
                // Incrémentez le compteur
                counter += 1;
                // Mettez à jour la valeur de votre élément avec arrondi
                divScoreFinalNum.innerHTML = counter;
                // Si le compteur atteint le score, arrêtez l'animation
                if (counter >= score) {
                    clearInterval(animationInterval);
                }
            }, interval);
        }
    }, 100);
}
    

// Fonction pour déplacer le bloc latéralement
function deplacerBloc(bloc, deltaX) {
    console.log("Déplacer bloc" + deltaX);

    let currentPosition = parseInt(bloc.style.left) || 0;
    let newPosition = currentPosition + deltaX;

    let tousLesBlocs = document.getElementsByClassName("bloc");
    let autresBlocs = Array.from(tousLesBlocs).filter(function(b) {
        return b !== bloc;
    })
    let blocCote = false;

    for (let i = 0; i < autresBlocs.length; i++) {
        let autreBloc = autresBlocs[i];
        // Vérification si les blocs se touchent
        let reponse = blocsVontSeToucheCote(bloc,autreBloc,deltaX);
        if (reponse){blocCote = reponse;}
    }

    // Vérifier que le bloc reste dans les limites du conteneur
    let conteneurWidth = divConteneur.offsetWidth;
    if (newPosition >= 0 && newPosition <= conteneurWidth - bloc.offsetWidth) {
        if (blocCote) {
            if (bloc.classList.contains('surprise')){
                bloc.innerHTML=tirerDans(tableauEnCours);
                bloc.classList.remove('surprise');
            }
            positionLeftBlocCote = blocCote.offsetLeft;
            bloc.style.left = positionLeftBlocCote - (20*deltaX) + 'px'; 
        } else {
            bloc.style.left = newPosition + 'px';
        }
    } else {
        if (newPosition < 0) {
            bloc.style.left = '0px'
        } else {
            bloc.style.left = conteneurWidth - 100 + 'px';
        }

    }
    verifierPosition(bloc,false);
}

function descendreBloc(bloc, deltaY) {
        let currentPosition = parseInt(bloc.style.top) || 0;
        let newPosition = currentPosition + deltaY;
        let tousLesBlocs = document.getElementsByClassName("bloc");
        let autresBlocs = Array.from(tousLesBlocs).filter(function(b) {
            return b !== bloc;
        })
        let blocDessous = false;

        for (let i = 0; i < autresBlocs.length; i++) {
            let autreBloc = autresBlocs[i];
            // Vérification si les blocs vont se toucher
            let reponse = blocsVontSeToucherDessus(bloc,autreBloc,deltaY);
            if (reponse) {
                blocDessous = reponse;
            }
        }


        let conteneurHeight = divConteneur.offsetHeight;

        // Le bloc n'est pas en bas de page.
        if (newPosition <= conteneurHeight - bloc.offsetHeight) {

            // Le bloc va toucher un autre bloc en-dessous
            if (blocDessous) {
                if (bloc.classList.contains('surprise')){
                    bloc.innerHTML=tirerDans(tableauEnCours);
                    bloc.classList.remove('surprise');
                }
                positionTopBlocDessous = blocDessous.offsetTop;   
                bloc.style.top = positionTopBlocDessous - 100 + 'px'; // On le pose dessus
                clearInterval(graviteInterval); // On arrête de descendre
                blocActif = null; // On déclare que ce bloc n'est plus actif

                if (positionTopBlocDessous < 100){ // Si on s'est posé sur un bloc trop haut ...             
                    console.log("perdu"); // ... on perd.
                    divConteneur.style.backgroundColor='red';
                    setTimeout(function() {
                        finDuJeu();
                    }, 500);
                }

                else { //Si on s'est posé sur un bloc suffisamment bas, on vérifie si un groupement est à faire.
                    verifierPosition(bloc,false);
                }
                
            // Si pas de bloc en-dessous, on continue.   
            } else {        
                bloc.style.top = newPosition + 'px';
            }

        // Le bloc est arrivé en bas de page
        } else {
            if (bloc.classList.contains('surprise')){
                bloc.innerHTML=tirerDans(tableauEnCours);
                bloc.classList.remove('surprise');
            }
            bloc.style.top = conteneurHeight - 100 + 'px';
            clearInterval(graviteInterval);
            blocActif = null;
            console.log("Lancement de la création de bloc");
            creeBloc();
        }
}

function gravite(bloc) {
    console.log('Gravité '+intervalle)
    // Appeler la fonction descendre toutes les 0.05 secondes
    graviteInterval = setInterval(function() {
        descendreBloc(bloc,vitesseDescente);
    }, intervalle);
}

function verifieTouche(bloc) {
    // Récupération de tous les objets de classe "bloc"
    let tousLesBlocs = document.getElementsByClassName("bloc");
    let autresBlocs = Array.from(tousLesBlocs).filter(function(b) {
        return b !== bloc;
    
    });
    console.log('Examen des '+autresBlocs.length+' blocs déjà présents')

    let autresBlocsQuiTouchent = [];
    // Parcours de tous les autres blocs
    for (let i = 0; i < autresBlocs.length; i++) {
        let autreBloc = autresBlocs[i];

        // Vérification si les blocs se touchent
        if (blocsSeTouchent(bloc, autreBloc)) {
            console.log(bloc.innerHTML+' touche '+autreBloc.innerHTML);
            autresBlocsQuiTouchent.push(autreBloc);
        } else {
            console.log(bloc.innerHTML+' ne touche pas '+autreBloc.innerHTML);
        }
    }
    if (autresBlocsQuiTouchent.length != 0) {
        return autresBlocsQuiTouchent;
    } else {
        return false;
    }
}

function verifierPosition(bloc,sansCreation) {
    console.log("verifierPosition "+bloc.innerHTML + " sansCreation "+sansCreation);
    let listeBlocTouche = verifieTouche(bloc);
    console.log('Liste des blocs touchés');
    console.log(listeBlocTouche);

    if (listeBlocTouche) { // Si le bloc touche un autre bloc ...
        console.log('Un ou des blocs sont touchés.');
        let blocTouche = complementaires(bloc, listeBlocTouche);
        if (blocTouche) { // S'ils sont complémentaires ...
            console.log('Complémentaires')
            if (!sansCreation){
                clearInterval(graviteInterval);
                blocActif = null;
            }
            bloc.classList.add('compatible');
            blocTouche.classList.add('compatible');
            if (effetsOn) {
                joueSon(sonBulle, false);
            }
            changeScore(10);

            let divBonus = document.createElement('div');
            divBonus.classList.add('bonus');
            divBonus.innerHTML = '+ 10';
            divBonus.style.left = bloc.offsetLeft + 'px';
            divBonus.style.top = bloc.offsetTop + 'px';
            divConteneur.appendChild(divBonus);
            divBonus.getBoundingClientRect();
            divBonus.style.opacity='0';
            divBonus.style.top= (bloc.offsetTop - 200) + 'px';
            setTimeout(function() {
                divBonus.remove();
            }, 3000); // 500 millisecondes (0.5 seconde)

            setTimeout(function () {
                Promise.all([
                    new Promise(resolve => {
                        bloc.remove();
                        resolve();
                    }),
                    new Promise(resolve => {
                        blocTouche.remove();
                        resolve();
                    })
                ]).then(function () {
                    setTimeout(function () {
                        supprimeBlocsIsoles().then(function () {
                            if (score < 100) {
                                console.log("Lancement de la création de bloc");
                                creeBloc();
                            }
                        });
                    }, 500); // 500 millisecondes (0.5 seconde)
                });
            }, 500); // 500 millisecondes (0.5 seconde)
            
        } else {
            console.log('Non complémentaires');
                if (!sansCreation){            
                console.log("Lancement de la création de bloc");
                creeBloc();
            }
        }
    }
}



// Fonction pour supprimer un élément et retourner une promesse résolue une fois la suppression terminée
function supprimeElement(element) {
    return new Promise((resolve, reject) => {
        element.remove();
        resolve();
    });
}


function toucheLeBas(bloc) {
    let currentPosition = parseInt(bloc.style.top) || 0;
    let hauteurConteneur = divConteneur.offsetHeight;
    let hauteurBloc = bloc.offsetHeight;
    return currentPosition + hauteurBloc >= hauteurConteneur;
}

function complementaires(bloc1,liste) {
    let complementaire = false;
    let valeurBloc1 = parseInt(bloc1.innerHTML);
    liste.forEach(bloc => {
        let valeurBloc2 = parseInt(bloc.innerHTML);
        if (valeurBloc1 + valeurBloc2 === total) {
            complementaire = bloc;

        }
    });
    return complementaire;
}

function supprimeBlocsIsoles() {
    return new Promise((resolve, reject) => {
        console.log("Début de la fonction Supprimer les blocs isolés");
        let tousLesBlocs = Array.from(document.getElementsByClassName("bloc"));
        let blocsConnectesAuBas = new Set();

         // Fonction pour vérifier la connexion d'un bloc avec les blocs connectés au bas
        function verifierConnexion(bloc) {
            let connecte = false;
            blocsConnectesAuBas.forEach(blocConnecte => {                
                if (blocsConnectesAuBas.has(verifieTouche(bloc, blocConnecte)[0])) {
                    connecte = true;
                }
            });
            return connecte;
        }

        // Examiner les blocs connectés au bas de l'écran
        tousLesBlocs.forEach(bloc => {
            if (toucheLeBas(bloc)) {
                blocsConnectesAuBas.add(bloc);
            }
        });

        let nouveauxConnectes;
        do {
            nouveauxConnectes = false;
            // Examiner les blocs restants pour vérifier leur connexion avec les blocs connectés au bas
            tousLesBlocs.forEach(bloc => {
                if (!blocsConnectesAuBas.has(bloc) && verifierConnexion(bloc)) {
                    blocsConnectesAuBas.add(bloc);
                    nouveauxConnectes = true;
                }
            });
        } while (nouveauxConnectes);

        let somme = 0;
        let moyenneX = [];
        let moyenneY = [];

        // Supprime les blocs qui ne sont pas connectés au bas de l'écran
        tousLesBlocs.forEach(bloc => {
            if (!blocsConnectesAuBas.has(bloc) && bloc != blocActif) {
                console.log('bloc actif ? '+bloc===blocActif)
                somme += parseInt(bloc.innerHTML);
                moyenneX.push(bloc.offsetLeft);
                moyenneY.push(bloc.offsetTop);
                bloc.classList.add('isole');
                console.log("Bloc isolé :", bloc.innerHTML);
                bloc.style.transition = 'all ease 1s';
                bloc.style.opacity='0';
                setTimeout(function() {
                    bloc.remove();
                }, 1000); // 500 millisecondes (0.5 seconde)
            }
        });

        if (somme > 0){

            if(effetsOn){joueSon(sonWoosh,false);}
            changeScore(somme * (10/total));

            let divBonus = document.createElement('div');
            divBonus.classList.add('bonus');
            divBonus.innerHTML = '+' + parseInt(somme * (10/total));
            divBonus.style.left = parseInt(calculerMoyenne(moyenneX)) + 'px';
            let positionY = parseInt(calculerMoyenne(moyenneY)) 
            divBonus.style.top = positionY + 'px';
            divConteneur.appendChild(divBonus);
            divBonus.getBoundingClientRect();
            divBonus.style.opacity='0';
            divBonus.style.top= (positionY - 200) + 'px';
            setTimeout(function() {
                divBonus.remove();
            }, 3000); // 500 millisecondes (0.5 seconde)
        }

        

        // Résoudre la promesse après la suppression des blocs isolés
        resolve();
    });
}


function calculerMoyenne(tableau) {
    let somme = 0;
    for (let i = 0; i < tableau.length; i++) {
        somme += tableau[i];
    }
    return somme / tableau.length;
}


function blocsVontSeToucheCote(bloc1,bloc2,x) {
    let rect1 = bloc1.getBoundingClientRect();
    let rect2 = bloc2.getBoundingClientRect();

    if ( (
        (rect1.right + x >= rect2.left &&
         rect1.left + x <= rect2.right   
        ) ||
        (rect1.left + x <= rect2.right &&
         rect1.right + x >= rect2.left   
        )
        ) &&  
        rect1.bottom >= rect2.top &&
        rect1.top <= rect2.bottom
    ) {return bloc2}
    else {return false}
}

function blocsVontSeToucherDessus(bloc1,bloc2,y) {
    let rect1 = bloc1.getBoundingClientRect();
    let rect2 = bloc2.getBoundingClientRect();
    if ( 
        rect1.bottom + y >= rect2.top &&
        rect1.top + y <= rect2.bottom &&   
        rect1.right > rect2.left &&
        rect1.left < rect2.right
    ) {

        return bloc2;}
    else {return false}
}



// Fonction utilitaire pour vérifier si deux blocs se touchent
function blocsSeTouchent(bloc1, bloc2,anticipation,x,y) {
    let rect1 = bloc1.getBoundingClientRect();
    let rect2 = bloc2.getBoundingClientRect();
    console.log(rect1);
    console.log(rect2);    
    
    if (anticipation){ 
        if (
            rect1.right + x >= rect2.left && 
            rect1.left + x <= rect2.right && 
            rect1.bottom + y >= rect2.top &&
            rect1.top + y <= rect2.bottom
        ) {return bloc2}
        else {return false}
    } else {
        return (
            rect1.right >= rect2.left && 
            rect1.left <= rect2.right && 
            Math.round(rect1.bottom) >= Math.round(rect2.top) &&
            rect1.top <= rect2.bottom
        );
    }


}

function pause() {
    if (jeuEnCours){
        if (blocActif && !pauseEnCours) {
            pauseEnCours = true;
            ouvre(divPause);
            clearInterval(graviteInterval);        
        } else {
            pauseEnCours = false;
            ferme(divPause);
            // Reprise du mouvement seulement si le jeu n'est pas en pause
            if (!pauseEnCours) {
                graviteInterval = setInterval(function() {
                    descendreBloc(blocActif,vitesseDescente);
                }, intervalle);
            }
        }
    }
}


function supprimerObjet(tableau, objetASupprimer) {
    const index = tableau.indexOf(objetASupprimer);
    if (index !== -1) {
        tableau.splice(index, 1);
    }
}

let keysPressed = {}; // Objet pour suivre les touches enfoncées
let intervalIds = {}; // Objet pour suivre les identifiants d'intervalle associés à chaque touche

// Écouteur d'événement pour les touches enfoncées
document.addEventListener('keydown', function(event) {
    if (!keysPressed[event.key] && (event.key === "ArrowLeft" || event.key === "ArrowRight")) {
        deplacementLateralEnCours = true;
        keysPressed[event.key] = true; // Ajouter la touche à l'objet keysPressed
        startRepeatingAction(event.key); // Démarrer l'action répétée

    } else {

        if (event.key === " ") {
            pause();
        }

        if (event.key === "Enter" && !jeuEnCours && pret) {
            ferme(divConsigne);
            ferme(divFin);
            debut(); // Commencer le jeu
        }

        if (event.key === "ArrowDown" && !acceleration) {
            console.log('Flèche bas enfoncée')
            acceleration=true;
            console.log('touche flèche bas')
            intervalle = 10;
            if (blocActif){
                clearInterval(graviteInterval); // Arrêter l'intervalle actuel
                graviteInterval = setInterval(function() {
                    descendreBloc(blocActif,vitesseDescente);
                }, intervalle);
            }         
        }
    }
});

// Écouteur d'événement pour les touches relâchées
document.addEventListener('keyup', function(event) {

    if (event.key=== 'ArrowDown' && acceleration) {
        console.log('Flèche bas relâchée')
        acceleration=false;        
        intervalle = 50;
        if (blocActif){
            clearInterval(graviteInterval); // Arrêter l'intervalle actuel
            graviteInterval = setInterval(function() {
                descendreBloc(blocActif,vitesseDescente);
            }, intervalle);
        }
    }

    else if (event.key==='ArrowLeft' || event.key==='ArrowRight') {
        deplacementLateralEnCours = false;
        delete keysPressed[event.key]; // Supprimer la touche de l'objet keysPressed
        stopRepeatingAction(event.key); // Arrêter l'action répétée
    }

});

// Fonction pour démarrer l'action répétée
function startRepeatingAction(key) {
    intervalIds[key] = setInterval(function() {
        handleKeyPress(key);
    }, 20); // Intervalles de 20 ms
}

// Fonction pour arrêter l'action répétée
function stopRepeatingAction(key) {
    clearInterval(intervalIds[key]);
}

// Fonction pour gérer les touches enfoncées
function handleKeyPress(key) {
    if (blocActif && !compatible) {
        if (key === "ArrowLeft") {
            deplacerBloc(blocActif, -5); // Déplacement à gauche        
        } else if (key === "ArrowRight") {
            deplacerBloc(blocActif, 5); // Déplacement à droite
        }        
    }

}


///////////// tactile ///////////////

// Variables pour suivre l'état du mouvement tactile
var startX = null;
var startY = null;
var isMoving = false;
var lastKeyCode = null;

// Fonction pour simuler l'événement keydown
function simulateKeyDown(key) {
    if (key !== lastKeyCode) {
        if (lastKeyCode !== null) {
            simulateKeyUp(lastKeyCode); // Simuler keyup sur le dernier bouton pressé
        }
        lastKeyCode = key;
        var event = new KeyboardEvent('keydown', {
            keyCode: key,
            key: key === 37 ? 'ArrowLeft' : key === 39 ? 'ArrowRight' : 'ArrowDown'
        });
        document.dispatchEvent(event);
    }
}

// Fonction pour simuler l'événement keyup
function simulateKeyUp(key) {
    lastKeyCode = null;
    var event = new KeyboardEvent('keyup', {
        keyCode: key,
        key: key === 37 ? 'ArrowLeft' : key === 39 ? 'ArrowRight' : 'ArrowDown'
    });
    document.dispatchEvent(event);
}

// Ajouter un gestionnaire d'événements pour détecter le début du mouvement tactile
document.addEventListener('touchstart', function(event) {
    startX = event.touches[0].clientX;
    startY = event.touches[0].clientY;
    isMoving = true;
});

// Ajouter un gestionnaire d'événements pour détecter la fin du mouvement tactile
document.addEventListener('touchend', function(event) {
    if (isMoving) {
        isMoving = false;
        if (lastKeyCode !== null) {
            simulateKeyUp(lastKeyCode); // Simuler keyup sur le dernier bouton pressé
        }
    }
});

// Ajouter un gestionnaire d'événements pour détecter le mouvement tactile
document.addEventListener('touchmove', function(event) {
    if (isMoving) {
        var endX = event.changedTouches[0].clientX;
        var endY = event.changedTouches[0].clientY;
        var deltaX = endX - startX;
        var deltaY = endY - startY;
        if (Math.abs(deltaX) > Math.abs(deltaY)) {
            if (deltaX > 0) {
                simulateKeyDown(39); // Simuler ArrowRight pour mouvement vers la droite
            } else {
                simulateKeyDown(37); // Simuler ArrowLeft pour mouvement vers la gauche
            }
        } else if (deltaY > 0) {
            simulateKeyDown(40); // Simuler ArrowDown pour mouvement vers le bas
        } else {
            if (lastKeyCode !== null) {
                simulateKeyUp(lastKeyCode); // Simuler keyup sur le dernier bouton pressé
            }
        }
    }
});
